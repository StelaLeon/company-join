// Databricks notebook source
import com.google.common.net.InternetDomainName
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}
import scala.util.Try
import org.apache.commons.text.similarity.LongestCommonSubsequence
import java.net.URL
import info.debatty.java.stringsimilarity._


/**
Please set up the following paths to the csv file containing the data sets
**/

val fileLocation = "/FileStore/tables/"
val fileType = "csv"
val GOOGLE_DS_PATH = fileLocation+"google_dataset.csv"
val FB_DS_PATH = fileLocation+"facebook_dataset.csv"
val WEBSITE_DS_PATH = fileLocation+"website_dataset.csv"

// CSV options
val inferSchema = "false"
val firstRowIsHeader = "false"
val delimiter = ","

/**
We define some utils functions to be able to process colum values as part of the cleanup 
**/

// COMMAND ----------

/**
For some data sets we have some less reliable fields (e.g. address in google data set is more accurate than the one in fb dataset)
**/
val mergeStringRightBiased = (s1: String, s2: String) => {
      // due to the fact that website_dataframe seems to have lower quality in the field address I will discard it unless s1 is null or empty
      if(s1==null || s1.isEmpty) s2 else s1
    } 


// COMMAND ----------

/**
We transform all urls into their root domain, or if error we get them as is
(!!!! Some more explanation in the clean up cell!!!)
**/
    val getDomainSuffix = (domain: String) => {
      val cleanDomain = if(domain.contains("http") || domain.contains("https")) domain else s"http://${domain}"
      val url = Try(InternetDomainName.from(new URL(cleanDomain).getHost).topPrivateDomain().toString)
      url.toOption.getOrElse(domain).replace("www.","")
    }

// COMMAND ----------

/**
We can get into the meaning of each cathegory to aggregate only "new"(in meaning) categories, 
but i have seen data is coming with duplicates categories so i assume we want to accumulate more variations     
**/
    val mergeCategories = (actual: String, toAdd: String) =>{
      if(actual == null) 
        s"$toAdd" 
      else if(toAdd == null) 
        s"$actual" 
      else 
        s"$actual | $toAdd" 
    }

// COMMAND ----------

/**
Since we join on domain(it is a column with 0% nulls) and phonenumber, we most probably get localized row
(phone number is per country/town/zipcode/etc) therefor I will treat this as localized with the precision of 
number of phone number column's nulls in both sides of the join, thefore at merging we would assume that the names with the highest similarity
will be typos or other noise from previous processing, while assuming the longest name is the most detailed 
**/
    val nameMerging = (name1: String, name2: String) => {
      if(name1==null && name2 == null) {
        ""
      }else if(name1==null)
        name2
      else if(name2==null)
        name1
      else {
        val jw = new JaroWinkler()
        jw.similarity(s"$name1", s"$name2") match {
          case high if (high > 0.7) => if (name1.length < name2.length) name2.toLowerCase() else name1.toLowerCase()
          // probably here we get similar name, let's take the longest one, name-differences based on country is not applicable, we are joining per (domain, phone number) 
          case average if (average <= 0.7 && average >= 0.3) => s"${name1.toLowerCase()} | ${name2.toLowerCase()}"
          //here i am not sure how to approach it, depends a lot on what we are doing with data further down the data pipeline, for now I will just concatenate it to have more details
          case low if (low < 0.3) => if (name1.length < name2.length) name1.toLowerCase() else name2.toLowerCase()
          // This is very dubious, since we got the same country here, most probably we have some rubbish long string and the real name in the other
          case 0 => if (name1.length < name2.length) name2.toLowerCase() else name1.toLowerCase()
          //this is when one of them is empty 
        }
      }
    }


// COMMAND ----------

import org.apache.spark.sql.types._
 
val googleDf = spark.read.option("header", "true").csv(GOOGLE_DS_PATH)
val fbDf = spark.read.option("header", "true").csv(FB_DS_PATH)
val websiteDf = spark.read.option("header", "true").option("delimiter",";").csv(WEBSITE_DS_PATH)

// COMMAND ----------

/**
When looking at the address field for both google and facebook dataset 
we see that there;s a higher percentage of null values in the facebook dataset 
as opposed to the google one. 

At first sight I can see also various alphabets in the fb dataset, making the data-reading dependent on the language
which follows to the idea that it must probably have some clean up before, resulting in loosing precision.
Therefore, since for the address it would be too complicated to compute differences and merging strategies of 2 different values, 
I will prefer google dataset address to the facebook one, unless the google ds address is null. 

I noticed various values in the address fields that seem to be coming from any other source but a source of addresses,
in order to merge these 2 columns with a cleaner data, we need a more complicated strategy than an intuitively biased method. 
**/

val gAddr = googleDf.select(
  col("address")
).filter("address is NULL").count()

val fAddr = fbDf.select(
  col("address")
).filter("address is NULL").count()

val gLen = googleDf.count()
val fLen = fbDf.count()
println(s"Fraction of null values for google is ${gAddr * 100 / gLen } %")
println(s"Fraction of null values for facebook is ${fAddr * 100 / fLen } %" )

googleDf.select(
  col("address")
).filter("address is NOT NULL").show(200)

fbDf.select(
  col("address")
).filter("address is NOT NULL").show(200)



// COMMAND ----------

//Debugging cells
googleDf.show()

// COMMAND ----------

fbDf.show()

// COMMAND ----------

websiteDf.show()

// COMMAND ----------


//Transform functions into UDFs

    val getDomainSufficUdf = udf(getDomainSuffix)
    val mergeCategoriesUdf = udf(mergeCategories)
    val mergeNamesUdf = udf(nameMerging)
    val mergeStringRightBiasedUdf = udf(mergeStringRightBiased)

// COMMAND ----------

//CLEAN UP THE DATA

/**
Clean up the data and align common values to a standard
- domain=> root domain, since we are join-ing on domain column 
           and some data sets, i noticed, it provides path+subdomain+root_domain urls
           i will allign all this data to be the root domain, 
           !!!! the google library spit some errors('Not under a public suffix') for some urls, 
           therefore the fawlty ones will be returned as is with no processing.... bummer!!!!!!
           I did not drop the fawlty urls because I randomly checked them and the pages were loading
           I will keep it simple for now and not discard them
           
- country,city => to Lower case this will be done at merge between 2 strings
**/

    val googleCleanedDf: DataFrame = googleDf.as("gdf").na.fill(" ").select(
        col("name").as("g_name"),
        col("domain"),
        col("phone"),
        col("category"),//.as("categories"),
        col("address").as("g_address"),
        col("city").as("g_city"),
        col("country_name").as("g_country"),
       // col("raw_address"),
       // col("region_name"),
       // col("zip_code"),
    ).withColumn("domain", getDomainSufficUdf(col("domain")))

    val fbCleanedDf = fbDf.na.fill(" ").select(
        col("name"),
        col("domain"),
        col("phone"),
        col("categories"),
        col("address"),
        col("city"),
        col("country_name").as("country"),
      //  col("region_name"),
      //  col("zip_code"),
    ).withColumn("domain", getDomainSufficUdf(col("domain")))


    val websiteCleanedDF = websiteDf.na.fill(" ").select(
        col("root_domain").as("domain"),
        col("phone"),
        col("site_name"),//.as("name"),
        col("s_category"),//.as("categories"),
        col("main_city"),//.as("city"),
        col("main_country"),//.as("country"),
        col("main_region").as("region")
    ).withColumn("domain", getDomainSufficUdf(col("domain")))


// COMMAND ----------

googleCleanedDf.show()

// COMMAND ----------

fbCleanedDf.show()

// COMMAND ----------

websiteCleanedDF.show()

// COMMAND ----------

/**
Perform the join while processing the common(in meaning) columns.
I clean up in between the joins so I can shuffle less data around. 
Unfortunatelly trying to print the execution plan, nothing got broadcasted to any node (used resources... tbd)
**/

    val mergedJoints = googleCleanedDf.alias("gdf")
      .join(fbCleanedDf.alias("fdf"), Seq("domain", "phone" ), "outer")
      .withColumn("categories", concat(
        when(col("gdf.category").isNull, col("fdf.categories")).otherwise(mergeCategoriesUdf(col("gdf.category"),col("fdf.categories")))
      ))
      .withColumn("name", concat(
        when(col("gdf.g_name").isNull, col("fdf.name")).otherwise(mergeNamesUdf(col("gdf.g_name"),col("fdf.name")))
      ))
      .withColumn("city", concat(
        when(col("gdf.g_city").isNull, col("fdf.city")).otherwise(mergeStringRightBiasedUdf(col("gdf.g_city"),col("fdf.city")))
      ))
      .withColumn("country", concat(
        when(col("gdf.g_country").isNull, col("fdf.country")).otherwise(mergeStringRightBiasedUdf(col("gdf.g_country"),col("fdf.country")))
      ))
      .withColumn("address", concat(
        when(col("gdf.g_address").isNull, col("fdf.address")).otherwise(mergeStringRightBiasedUdf(col("gdf.g_address"),col("fdf.address")))
      ))
      .drop("category")
      .drop("g_name")
      .drop("g_city")
      .drop("g_country")
      .drop("g_address")
      .dropDuplicates()
      .alias("fgdf")
      .join(websiteCleanedDF.alias("wdf"), Seq("domain", "phone" ), "outer")
      .withColumn("categories", concat(
        when(col("categories").isNull, col("wdf.s_category"))
        .when(col("wdf.s_category").isNull,col("categories"))
        .otherwise(mergeCategoriesUdf(col("categories"),col("wdf.s_category")))
      ))
      .withColumn("name", concat(
        when(col("wdf.site_name").isNull, col("fgdf.name")).otherwise(mergeNamesUdf(col("wdf.site_name"),col("fgdf.name")))
      ))
      .withColumn("city", concat(
        when(col("wdf.main_city").isNull, col("fgdf.city")).otherwise(mergeStringRightBiasedUdf(col("wdf.main_city"),col("fgdf.city")))
      ))
      .withColumn("country", concat(
        when(col("wdf.main_country").isNull, col("fgdf.country")).otherwise(mergeStringRightBiasedUdf(col("wdf.main_country"),col("fgdf.country")))
      ))
     .drop("s_category")
     .drop("site_name")
     .drop("main_city")
     .drop("main_country")
     .dropDuplicates()
      //.explain(extended = false)

    mergedJoints.show(200)

// COMMAND ----------

// DBTITLE 1,Future optimization
/**
Future improvements: 
1. Analyse the dataframes execution plan and reduce the size of datasets by splitting the current ones into multiple smaller-sub-datasets that can be broadcasted, at this point i ran this only in a single node cluster and this hasn't been tested, 
2. Data scewness: I choose the \domain\ column as this is the column that has 0% null values, therefore we don;t end up with the same key resulting in an increase data shuffling
3.  phone number is part of the join as well, it has a higher percentage of null values therefore it would be a nice step to simply improve this, by simply generating a non-valid value with a random-number/string in order to be able to scatter the data better, making it faster. 
4. !!!!! Get rid of UDFs: this is a very important and simple step!!!! The spark optimizer does not have any clues about UDFs, i used UDFs in my code as personally it was more handy for me, but
some of the string manipulation UDFs can be translated in when().when().otherwise() // in fairness, once I plugged in all the UDFs I noticed an almost double execution time for joins.
**/
